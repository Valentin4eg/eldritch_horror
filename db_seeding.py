from eldritch_chronicle.models import Prelude, Ancient
from eldritch_chronicle import db

ANCIENTS_DICT = {"Abhoth": ["Epidemic", 11, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/4/46/Abhoth.png/'],
"Antediluvium": ["The Stars Align", 37, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/c/c4/Antediluvium.png/'],
"Atlach-Nacha": ["Web Between Worlds", 24, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/2/28/Atlach-Nacha.png'],
"Azathoth": ["Beginning of the End", 4, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/9/9e/Azathoth.png/'],
"Cthulhu": ["Call of Cthulhu", 9, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/9/9e/Cthulhu.png/'],
"Hastur": ["The King in Yellow", 16, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/2/27/Hastur.png/'],
"Hypnos": ["Otherworldly Dreams", 25, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/7/71/Hypnos.png'],
"Ithaqua": ["Rumors from the North", 1, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/1/16/Ithaqua.png/'],
"Nephren-Ka": ["Under the Pyramids", 12, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/c/c9/Nephren-Ka.png/'],
"Nyarlathotep": ["Harbinger of the Outer Gods", 35, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/8/89/Nyarlathotep.png/'],
"Rise of the Elder Things": ["Doomsayer from Antarctica", 5, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/6/62/Elder_Things.png/'],
"Shub-Niggurath": ["Twin Blasphemies of the Black Goat", 23, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/7/70/Shub-Niggurath.png/'],
"Shudde M’ell": ["Apocalypse Nigh", 27, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/0/0e/Shudde_Mell.png/'],
"Syzygy": ["In Cosmic Alignment", 20, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/9/9c/Syzygy.png/'],
"Yig": ["Father of Serpents", 34, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/7/7f/Yig.png/'],
"Yog-Sothoth": ["The Dunwich Horror", 19, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/4/45/Yog-Sothoth.png/']}

RAW_LIST_OF_PRELUDES = ["Rumors from the North",
    "Key to Salvation", "Unwilling Sacrifice",
    "Beginning of the End", "Doomsayer from Antarctica",
    "Ultimate Sacrifice", "Ghost from The Past",
    "Drastic Measures", "Call of Cthulhu",
    "Litany of Secrets", "Epidemic",
    "Under the Pyramids", "Weakness to Strength",
    "Sins of the Past", "Silver Twilight Stockpile",
    "The King in Yellow", "Dark Blessings",
    "The Coming Storm", "The Dunwich Horror",
    "In Cosmic Alignment", "Written in the Stars",
    "Lurker Among Us", "Twin Blasphemies of the Black Goat",
    "Web Between Worlds", "Otherworldly Dreams",
    "Focused Training", "Apocalypse Nigh",
    "Fall of Man", "The Price of Prestige",
    "You Know What You Must Do", "Aid of the Elder Gods",
    "The Archives", "Army of Darkness",
    "Father of Serpents", "Harbinger of the Outer Gods",
    "In the Lightless Chamber", "The Stars Align",
    "Temptation", "Unto the Breach", "Wondrous Curios"]

def seed_preludes():
    for prelude in RAW_LIST_OF_PRELUDES:
        prelude_obj = Prelude(name=prelude,
                              basic_effect='Basic prelude effect (not native)',
                              special_effect='Special prelude effect (native)')
        db.session.add(prelude_obj)
        db.session.commit()

def seed_ancients():
    for k, v in ANCIENTS_DICT.items():
        ancient_obj = Ancient(name=k,
                              prelude_id=v[1],
                              image_url=v[2])
        db.session.add(ancient_obj)
        db.session.commit()
