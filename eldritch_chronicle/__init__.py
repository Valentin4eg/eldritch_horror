import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager


DATABASE_URL = os.getenv('HEROKU_POSTGRES')

app = Flask(__name__)

app.config.from_pyfile('settings.py')
app.config['SECRET_KEY'] = os.getenv('SECRET_KEY')
app.config['WTF_CSRF_SECRET_KEY'] = os.getenv('WTF_CSRF_SECRET_KEY')
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('HEROKU_POSTGRES')

db = SQLAlchemy(app)

bcrypt = Bcrypt(app)

login_manager = LoginManager(app)
login_manager.login_view = 'login'
login_manager.login_message_category = 'info'

from eldritch_chronicle import routes